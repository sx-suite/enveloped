[![MPL 2.0 License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<br />
<br />
<div align="center">
  <a href="https://gitlab.com/sx-suite/enveloped">
    <img src="https://gitlab.com/sx-suite/enveloped/-/raw/main/misc/icon.png" alt="Logo" width="80" height="80">
  </a>

  <br/>
  <br/>

  <div align="center">
    <h3 align="center">Enveloped</h3>
  </div>

  <div align="center">
	Extended parser for .env file. JSON interpreter, nested keys and some form of encryption
    <br/>
    <br/>
    <a href="https://gitlab.com/sx-suite/enveloped"><strong>Ir a la página</strong></a>
  </div>
</div>
<br />

---

<br/>

_Enveloped_ is an alternative for `dotenv`, more feature-rich out of the box, with pretty simple configurations.

## Requirements

At the moment, _Enveloped_ it's a zero-dependencies package.

**The .env file must be in JSON**

## Installation and usage

In an initialized project, run:

```
npm i enveloped
```

Then, on the main file, add this lines:

```javascript
import enveloped from "enveloped";
enveloped.init();
```

I strongly recommend to put them at the top of all.

Now, every time you need an env variable, use the `process.var` function:

```javascript
/* .env content
{
	"CURRENT": {
		"STAGE": "testing"
	}
}
*/

let envStage1 = process.var("CURRENT.STAGE");
console.log(envStage1); //-> testing

// Also valid

envStage = process.var("CURRENT").STAGE;
const { STAGE } = process.var("CURRENT");
```

## Configuration

You can pass a custom configuration through `enveloped.init(config)`, where config is an object with:

-   **`rootPath`**: directory path where the `.env` file/s are located. (default: `process.cwd()`)
-   **`filename`**: JSON env file name. (default: `.env`)
-   **`type`**: if `filename` is not supplied, this property will load append to the file name a type. It's a nice-to-have when working with different env files, depending on the stage. (default: _empty string_) (example: `type: "development"`, loaded file: `.env.development`)
-   **`secretKey`**: the secret key used to encrypt the `.env` content. It must have only 32 characters (default: it generates a random on initialization).
-   **`debugMode`**: if something is not working correctly, this mode will output some help logs to the console. (default: `false`)

All of this properties are optional. It's important to notice that `type` will be ignored if `filename` is supplied.

## How it works?

When you call `enveloped.init()`, the script will:

1. Merge the configs, in case you supplied a custom one.
2. Setup `debugMode` if it's true.
3. Configure the `process` node object, adding the `var` function.
4. Setup the secret key for the encryption.
5. Get, validate and parse the env file.
6. Encrypt a minified version of the env file to a hash.
7. Save the hash in a env variable called `NODE_ENVELOPED`.

As you can see, there will only be one env variable, containing a hash.

Now, every time you call `process.var("<varName>")`, it will:

1. Read the `NODE_ENVELOPED` hash.
2. Decrypt it and convert it to an object.
3. Search the var.
4. Return the value if it finds it, or null if not.

Of course, the `varName` can be chained with dots like: `"KEY.CHAINED"`, to refer a nested key.

For an in-depth guide or reference, you can visit the [Wiki][wiki-url].

## Licence

_Enveloped_ is under **`Mozilla Public Licence 2.0`** so, obviously, it is open-source. More info at [Mozilla MPL 2.0][license-url]

---

_Made by Juan Saez_

<!-- Variables -->

[license-shield]: https://img.shields.io/badge/license-MPL_2.0-orange?style=for-the-badge&logo=mozilla
[license-url]: https://www.mozilla.org/en-US/MPL/2.0/
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/juan-saez-18525a16b/
[wiki-url]: https://gitlab.com/sx-suite/enveloped/-/wikis/home
