export interface EnvelopedConfig {
	rootPath: string;
	filename: string;
	type: string;
	secretKey: string;
	debugMode: boolean;
}

export interface PartialEnvelopedConfig {
	rootPath?: string;
	filename?: string;
	type?: string;
	secretKey?: string;
	debugMode?: boolean;
}
