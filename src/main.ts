import fs from "fs";
import { EnvelopedConfig, PartialEnvelopedConfig } from "./types";
import { getDebug, getFilePath, getConfig } from "./utils";
import { encrypt, encryptionConfig } from "./encrypt";
import { configureProcessObj } from "./process";

export const init = (c?: PartialEnvelopedConfig) => {
	const config: EnvelopedConfig = getConfig(c);

	// == Set debug mode

	const debug = getDebug(config);

	// == Setup process extensions

	configureProcessObj();

	// == Set secret key

	if (config.secretKey.length !== 32)
		return debug(
			"The !{fgMagenta}secretKey!{reset} must have 32 characters",
			false
		);

	encryptionConfig.setSecretKey(config.secretKey);

	debug(
		`Current secret key: !{fgYellow}${encryptionConfig.secretKey}!{reset}`
	);

	// == Get, validate and parse filepath

	const filepath = getFilePath(config);

	if (!fs.existsSync(filepath)) {
		debug(`File !{fgCyan}${config.filename}!{reset} not found`, false);
		return;
	}

	const fileContent = fs.readFileSync(filepath).toString("utf-8");

	// == Check JSON errors

	let jsonObj = {};
	try {
		jsonObj = JSON.parse(fileContent);
	} catch (e) {
		debug("env JSON malformed", false);
		return;
	}

	debug(`File !{fgCyan}${config.filename}!{reset} readed successfully`);

	// == Hash vars and save to env

	const minified = JSON.stringify(jsonObj);
	const hashedContent = encrypt(minified);
	process.env.NODE_ENVELOPED = hashedContent;

	debug("Env vars encrypted and saved successfully");
};

module.exports = { init };
export default module.exports;
