import path from "path";
import { init } from "../main";
import { configureProcessObj, getVar } from "../process";

describe("process.ts", () => {
	it("should add var function to process", () => {
		configureProcessObj();
		expect(process).toHaveProperty("var", getVar);
	});

	const initEnveloped = (filename: string) =>
		init({
			rootPath: path.resolve(__dirname, "envs"),
			filename
		});

	describe("process on execution", () => {
		it("should be a hash on env", () => {
			initEnveloped("ok.json");
			expect(process.env.NODE_ENVELOPED).toBeDefined();
		});

		it("should load properly", () => {
			initEnveloped("ok.json");
			expect(process.var("IS_OK")).toBeTruthy();
		});

		it("should load nested keys", () => {
			initEnveloped("nested.json");
			expect(process.var("TESTING.NESTED")).toHaveProperty("KEYS");
			expect(process.var("TESTING.NESTED")).toHaveProperty("WORKING");
		});

		it("should load arrays", () => {
			initEnveloped("nested.json");
			expect(process.var("TESTING.ARRAY")).toHaveLength(5);
			expect(process.var("TESTING.ARRAY.2")).toBe(3);
		});

		it("should not load malformed json", () => {
			initEnveloped("malformed.json");
			expect(process.env.NODE_ENVELOPED).toBeUndefined();
		});

		it("should return null on non-existent key", () => {
			initEnveloped("ok.json");
			expect(process.var("NON_EXISTENT")).toBeNull();
		});

		it("should return null on non-existent key (but nested)", () => {
			initEnveloped("nested.json");
			expect(process.var("TESTING.NESTED.NULL")).toBeNull();
		});

		it("should return null on uninitialized enveloped", () => {
			expect(process.var("SOME_KEY")).toBeNull();
		});

		afterEach(() => delete process.env.NODE_ENVELOPED);
	});
});
