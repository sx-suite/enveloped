import path from "path";
import { getFilePath, defaultConfig, getConfig, getDebug } from "../utils";
import { formats } from "../debugger";

describe("utils.ts", () => {
	describe("getConfig function", () => {
		it("should return defaults", () => {
			const c = getConfig();
			expect(c).toEqual(defaultConfig);
		});

		it("should only change type", () => {
			const c = getConfig({ type: "testing" });
			expect(c.type).toBe("testing");
			expect(c.debugMode).toBeFalsy();
		});

		it("should only change debugMode", () => {
			const c = getConfig({ debugMode: true });
			expect(c.type).toBe("");
			expect(c.debugMode).toBeTruthy();
		});
	});

	describe("getFilePath function", () => {
		it("should get it properly", () => {
			const filepath = getFilePath(defaultConfig);
			expect(filepath).toBe(
				path.join(defaultConfig.rootPath, defaultConfig.filename)
			);
		});

		it("should get it with type", () => {
			const c = getConfig({ type: "test" });
			const filepath = getFilePath(c);

			expect(filepath).toBe(
				path.join(
					defaultConfig.rootPath,
					`${defaultConfig.filename}.test`
				)
			);
		});

		it("should get it with different filename", () => {
			const c = getConfig({ filename: "test" });
			const filepath = getFilePath(c);

			expect(filepath).toBe(path.join(defaultConfig.rootPath, "test"));
		});

		it("should prioritize filename over type", () => {
			const c = getConfig({ filename: "test", type: "ignored" });
			const filepath = getFilePath(c);

			expect(filepath).toBe(path.join(defaultConfig.rootPath, "test"));
		});
	});

	describe("getDebug function", () => {
		const originalLog = console.log;

		let logHistory: string[] = [];
		const mockedLog = (...input: any) => (logHistory = input);

		beforeEach(() => (console.log = mockedLog));
		afterEach(() => {
			console.log = originalLog;
			logHistory = [];
		});

		it("should print a message", () => {
			const c = getConfig({ debugMode: true });
			const debug = getDebug(c);

			debug("hi!");

			expect(logHistory[0]).toContain("hi!");
			expect(logHistory[0]).toContain(formats.fgGreen);
		});

		it("should not print a message", () => {
			const c = getConfig({ debugMode: false });
			const debug = getDebug(c);

			debug("hi!");

			expect(logHistory).toHaveLength(0);
		});

		it("should print error message", () => {
			const c = getConfig({ debugMode: true });
			const debug = getDebug(c);

			debug("error", false);

			expect(logHistory[0]).toContain("error");
			expect(logHistory[0]).toContain(formats.fgRed);
		});
	});
});
