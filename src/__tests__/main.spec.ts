import path from "path";
import { init } from "../main";

describe("init.ts", () => {
	afterEach(() => delete process.env.NODE_ENVELOPED);

	it("should save hash on NODE_ENVELOPED", () => {
		init({
			rootPath: path.resolve(__dirname, "envs"),
			filename: "ok.json"
		});
		expect(process.env.NODE_ENVELOPED).toBeDefined();
	});

	it("should not save hash; bad secretKey", () => {
		init({ secretKey: "test" });
		expect(process.env.NODE_ENVELOPED).toBeUndefined();
	});

	it("should call elemental functions", () => {
		init();
		expect(process).toHaveProperty("var");
	});

	it("should an print error of not found file", () => {
		init({ filename: "non-existent.json" });
		expect(process.env.NODE_ENVELOPED).toBeUndefined();
	});
});
