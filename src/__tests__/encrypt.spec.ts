import { generateHash, encrypt, decrypt, encryptionConfig } from "../encrypt";

describe("encrypt.ts", () => {
	it("should update secretKey on config", () => {
		encryptionConfig.setSecretKey("hi!");
		expect(encryptionConfig.secretKey).toBe("hi!");
	});

	beforeEach(() => encryptionConfig.setSecretKey(generateHash(32)));

	describe("generateHash function", () => {
		it.each([5, 20, 32, 100])(
			"should generate a hash with n chars",
			(n) => {
				const hash = generateHash(n);
				expect(hash).toHaveLength(n);
			}
		);

		it("should return empty string on negative n", () => {
			const hash = generateHash(-1);
			expect(hash).toBe("");
		});

		it("should be only the supported chars", () => {
			const chars =
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			const hash = generateHash(1000);

			let supported = true;
			for (const l of hash) {
				if (!chars.includes(l)) {
					supported = false;
					break;
				}
			}

			expect(supported).toBeTruthy();
		});
	});

	describe("encrypt function", () => {
		it("should encrypt an string properly", () => {
			const hash = encrypt("hi!");
			expect(hash.length).toBeGreaterThanOrEqual(1);
		});

		it("should return empty string", () => {
			const hash = encrypt("");
			expect(hash).toBe("");
		});
	});

	describe("decrypt function", () => {
		it("should decrypt an string properly", () => {
			const hash = encrypt("hi!");
			const str = decrypt(hash);
			expect(str).toBe("hi!");
		});

		it("should return empty string", () => {
			const hash = decrypt("");
			expect(hash).toBe("");
		});
	});
});
