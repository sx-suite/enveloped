import crypto from "crypto";

export const generateHash = (c: number) => {
	if (c < 0) return "";

	const chars =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	let result = "";
	for (let i = 0; i < c; i++) {
		const index = Math.floor(Math.random() * chars.length);
		result += chars[index];
	}

	return result;
};

export const encryptionConfig = {
	algorithm: "aes-256-ctr",
	secretKey: generateHash(32),
	iv: crypto.randomBytes(16),
	setSecretKey: (k: string) => (encryptionConfig.secretKey = k)
};

export const encrypt = (v: string) => {
	if (v === "") return "";

	const { algorithm, secretKey, iv } = encryptionConfig;
	const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
	return Buffer.concat([cipher.update(v), cipher.final()]).toString("hex");
};

export const decrypt = (h: string) => {
	if (h === "") return "";

	const { algorithm, secretKey, iv } = encryptionConfig;
	const decipher = crypto.createDecipheriv(
		algorithm,
		secretKey,
		Buffer.from(iv.toString("hex"), "hex")
	);

	return Buffer.concat([
		decipher.update(Buffer.from(h, "hex")),
		decipher.final()
	]).toString();
};
