import path from "path";
import { log } from "./debugger";
import { EnvelopedConfig, PartialEnvelopedConfig } from "./types";
import { generateHash } from "./encrypt";

export const getFilePath = (config: EnvelopedConfig) => {
	const { type, filename, rootPath } = config;

	let fn = filename;
	if (fn === ".env" && type !== "") fn = `.env.${type}`;

	return path.resolve(rootPath, fn);
};

export const getDebug =
	(config: EnvelopedConfig) =>
	(msg: string, ok = true) =>
		config.debugMode &&
		log(
			`!{dim}[enveloped]!{reset} !{${
				ok ? "fgGreen" : "fgRed"
			}}»!{reset} ${msg}`
		);

export const defaultConfig = {
	type: "",
	filename: ".env",
	rootPath: path.resolve(process.cwd()),
	debugMode: false,
	secretKey: generateHash(32)
};

export const getConfig = (c?: PartialEnvelopedConfig) => ({
	...defaultConfig,
	...c
});
