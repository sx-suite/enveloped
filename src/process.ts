import { decrypt } from "./encrypt";

export const configureProcessObj = () => {
	process = Object.assign(process, { var: getVar });
};

export function getVar(v: string): string | null {
	const hashed = process.env.NODE_ENVELOPED;

	if (typeof hashed === "undefined" || typeof v === "undefined" || v === null)
		return null;

	const decryptedHash = decrypt(hashed);
	const envobj = JSON.parse(decryptedHash);

	const chain = v.split(".");
	let innerObj = envobj;

	for (const k of chain) {
		if (!(k in innerObj)) return null;

		innerObj = innerObj[k];
	}

	return innerObj;
}
